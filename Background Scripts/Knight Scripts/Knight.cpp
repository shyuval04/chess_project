#include "Knight.h"
#include "Board.h"


Knight::Knight()
{

}

Knight::~Knight()
{

}

/*
checks if Knight is able to reach
In: the source and destenation
Out: the way she passes
*/
int Knight::isAbleToReach(string src, string dest, Board b)
{
	int flag = 0;
	int pos = 0;

	string* way = calcWay(src, dest);
	string cpy = src;


	if ( (this->_moveVector[0] == 2 &&  this->_moveVector[1] == 1) == false && (this->_moveVector[0] == 1 && this->_moveVector[1] == 2) == false)
	{
		return 6;
	}
	
	char posY = dest.at(1);
	char posX = dest.at(0);

	
	
	string stringToCheck = "/0";
	
	stringToCheck = string() + posX + posY + string();
	char pieceToCheck = b.getPieceInfo(stringToCheck);
	if (pieceToCheck >= 97 && pieceToCheck <= 122 && !(this->color))
	{
		return 6;
	}
	
	if (pieceToCheck >= 65 && pieceToCheck <= 90 && (this->color))
	{
		return 6;
	}


	return 0;
}

/*
checks if the Knight aims the king
in: the destintion and the board
out: if this Knight is aiming the  king
*/
bool Knight::isAimingKing(string src, Board b)
{
	bool IsAiming = false;
	char i = 0;

	char posY = src.at(1);
	char posX = src.at(0);

	char posYcpy = posY + 1;
	char posXcpy = posX + 1;

	if (sendPeaceToCheck(posX + 2, posY + 1, b))
	{
		return true;
	}

	
	if (sendPeaceToCheck(posX + 2, posY - 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX + 2, posY + 2, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX + 2, posY - 2, b))
	{
		return true;
	}
	




	if (sendPeaceToCheck(posX - 2, posY + 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 2, posY - 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 2, posY + 2, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 2, posY - 2, b))
	{
		return true;
	}
	
	





	if (sendPeaceToCheck(posX + 1, posY + 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX + 1, posY - 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX + 1, posY + 2, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX + 1, posY - 2, b))
	{
		return true;
	}







	if (sendPeaceToCheck(posX - 1, posY + 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 1, posY - 1, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 1, posY + 2, b))
	{
		return true;
	}

	if (sendPeaceToCheck(posX - 1, posY - 2, b))
	{
		return true;
	}
	
	return false;
}



/*
this function checks if the Knight aims the king
in: the destintion and the board
out: if this Knight is aiming the  king
*/
bool Knight::sendPeaceToCheck(char posXcpy, char posYcpy, Board b)
{
	char kingTocheck = (this->_color) ? 'K' : 'k';
	string stringToCheck = "/0";
	char pieceToCheck = 0;
	

	stringToCheck = string() + posXcpy + posYcpy + string();
	pieceToCheck = b.getPieceInfo(stringToCheck);
	if (pieceToCheck == kingTocheck)
	{
		return true;

	}
	
	return false;
}







