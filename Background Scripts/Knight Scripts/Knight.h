#pragma once
#include "BasicPawn.h"

class Knight : public BasicPawn
{
public:
	Knight();
	~Knight();

	int isAbleToReach(string src, string dest, Board b);
	bool isAimingKing(string src, Board b);//if Aiming the king
	bool sendPeaceToCheck(char posXcpy, char posYcpy, Board b);//send the peace to check
};
#pragma once