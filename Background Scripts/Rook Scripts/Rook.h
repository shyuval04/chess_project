#pragma once

#include "BasicPawn.h"

class Rook : public BasicPawn
{
public:
	Rook();
	~Rook();

	bool isAimingKing(string src, Board b);
	int isAbleToReach(string src, string dest, Board b);

private:

};


