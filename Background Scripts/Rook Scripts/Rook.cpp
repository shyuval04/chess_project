#include "Rook.h"

Rook::Rook()
{
}

Rook::~Rook()
{
}

/*
checks if piece is able to reach
In: the source and destenation
Out: the way she passes
*/
int Rook::isAbleToReach(string src, string dest, Board b)
{
	int flag = 0;
	int pos = 0;

	string* way = calcWay(src, dest);
	string cpy = src;

	std::cout << "The Way This Rook Does it: \n" << way << std::endl;

	if ((this->_moveVector[0] != 0) && (this->_moveVector[1] != 0))
	{
		flag = 6;
	}
	else
	{
		while (pos < (way->size() - AXISES_AMOUNT)) //in order to ignore the last piece(in case he is eating)
		{
			cpy = way->substr(pos, AXISES_AMOUNT);


			if (isalpha(b.getPieceInfo(cpy)))
			{
				flag = 6;
				break;
			}
			pos += AXISES_AMOUNT;
		}

	}

	return flag;
}

/*
checks if this rook aims the king
in: current pos and board
out: is aiming king
*/
bool Rook::isAimingKing(string src, Board b)
{
	bool IsAiming = false;

	char kingTocheck = (this->_color) ? 'K' : 'k';
	char i = 0;

	char posY = src.at(1);
	char posX = src.at(0);

	char posYcpy = posY + 1;
	char posXcpy = posX + 1;

	string stringToCheck = "/0";
	char pieceToCheck = 0;

	while (--posXcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";
	posXcpy = posX - 1;

	while (++posXcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";
	
	while (--posYcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";
	posYcpy = posY - 1;

	while (++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	

	/*
	for (char i = '1'; i <= '8' && !IsAiming; i++)	//checks col
	{
		stringToCheck = string() + posY + i + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha)
		{

		}
	}

	stringToCheck = "/0";
	pieceToCheck = 0;
	
	for (char i = 'a'; i <= 'h' && !IsAiming; i++)	//checks row
	{
		stringToCheck = string() + i + posX + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
	}
	*/
	return IsAiming;
}