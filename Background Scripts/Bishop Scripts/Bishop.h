#pragma once
#include "BasicPawn.h"

class Bishop : public BasicPawn
{
public:
	Bishop();
	~Bishop();

	int isAbleToReach(string src, string dest, Board b);
	bool isAimingKing(string src, Board b);

};
#pragma once