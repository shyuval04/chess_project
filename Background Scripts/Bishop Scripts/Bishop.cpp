#include "Bishop.h"
#include "Board.h"

Bishop::Bishop()
{

}

Bishop::~Bishop()
{

}


/*
checks if piece is able to reach
In: the source and destenation and the board
Out: if it's A legal move
*/
int Bishop::isAbleToReach(string src, string dest, Board b)
{
	int flag = 0;
	int pos = 0;

	string* way = calcWay(src, dest);
	string cpy = src;


	if (this->_moveVector[0] != this->_moveVector[1])
	{
		return 6;
	}

	else
	{
		while (pos < (way->size() - AXISES_AMOUNT)) //in order to ignore the last piece(in case he is eating)
		{
			cpy = way->substr(pos, AXISES_AMOUNT);


			if (isalpha(b.getPieceInfo(cpy)))
			{
				return 6;
			}
			pos += AXISES_AMOUNT;
		}

	}

	return 0;
}

/*
checks if the Bishop aims the king
in: the destintion and the board
out: if this Knight is aiming the  king
*/

bool Bishop::isAimingKing(string src, Board b)
{
	bool IsAiming = false;

	char kingTocheck = (this->_color) ? 'K' : 'k';
	char i = 0;

	char posY = src.at(1);
	char posX = src.at(0);

	char posYcpy = posY + 1;
	char posXcpy = posX + 1;

	string stringToCheck = "/0";
	char pieceToCheck = 0;

	//Diagonal right up
	posYcpy = posY - 1;
	posXcpy = posX - 1;

	while (++posXcpy <= '8' && ++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	//Diagonal down right

	posXcpy = posX - 1;
	posYcpy = posY + 1;
	while (++posXcpy <= '8' && --posYcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	//Diagonal up and left

	posXcpy = posX - 1;
	posYcpy = posY + 1;
	while (--posXcpy >= '1' && ++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	//Diagonal down to the left
	posXcpy = posX - 1;
	posYcpy = posY - 1;
	while (--posXcpy >= '1' && --posYcpy <= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	return IsAiming;
}