#pragma once
#include <iostream>

#include <ctype.h>

#define GAME_START_SETUP_STRING "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR"
#define GAME_CHECK_SETUP_STRING "pppppppprnbkqbnr################################RNBKQBNRPPPPPPPP"

#define ANSWER_LENGTH 66
#define BOARD_SIZE 8

using std::string;

class Board
{
private:

	//fields:

	char _board[BOARD_SIZE][BOARD_SIZE] = {
	{ 'r','n','b','k','q','b','n','r' },
	{ 'p','p','p','p','p','p','p','p' },
	{ '#','#','#','#','#','#','#','#' },
	{ '#','#','#','#','#','#','#','#' },
	{ '#','#','#','#','#','#','#','#' },
	{ '#','#','#','#','#','#','#','#' },
	{ 'P','P','P','P','P','P','P','P' },
	{ 'R','N','B','K','Q','B','N','R' }
	};

	bool  _isWhiteTurn = true;
	string _boardState = GAME_CHECK_SETUP_STRING;
	int _moveStat;//legal/illegal

public:

	Board();
	~Board();

	string getBoardState();

	string setup();
	void move(string src, string dest);
	char* getAnswer(string msg);
	char getPieceInfo(string pos);

	void UpdateBoard();
	void PrintBoard();
	void kills(int locationX, int locationY);
};
#pragma once
