#include "Board.h"
#include "BasicPawn.h"
#include "King.h"
#include "Rook.h"

Board::Board()
{

}

Board::~Board()
{

}

char* Board::getAnswer(string msg)
{
	Rook rook;
	King king;
	char ans[1024] = "0";

	bool isStupidMove = false;

	string src = msg.substr(0, 2);//source 
	string dest = msg.substr(2, 2);//dest 

	char srcPiece = Board::getPieceInfo(src);
	char dstPiece = 0;

	if (!isalpha(srcPiece))
	{
		strcpy_s(ans, "5");
		isStupidMove = true;
	}
	//minimize disorder
	else
	{
		dstPiece = Board::getPieceInfo(dest);
		
		if (srcPiece == 'r' || srcPiece == 'R')
		{
			_itoa_s(rook.isAbleToReach(src, dest, *this), ans, 10); //check illegal move
		}
		if (srcPiece == 'k' || srcPiece == 'K')
		{
			_itoa_s(king.isAbleToReach(src, dest, *this), ans, 10);
		}
	}

	if (!strcmp(ans, "6") || isStupidMove)
	{

	}
	else if ((this->_isWhiteTurn && islower(dstPiece)) || (!this->_isWhiteTurn && isupper(dstPiece)))
	{//its the player's turn
		strcpy_s(ans, "3");
	}
	else if (srcPiece == dstPiece && isalpha(srcPiece) && isalpha(dstPiece)) //dont wanna take risks
	{// source is dest
		strcpy_s(ans, "7");
	}
	else
	{

		if ((isupper(srcPiece) && this->_isWhiteTurn) || (islower(srcPiece) && !(this->_isWhiteTurn)))
		{
			strcpy_s(ans, "2");
		}
		else if (rook.isAimingKing(dest, *this))
		{
			strcpy_s(ans, "1");
		}
		else if (king.isAimingKing(dest, *this) && (srcPiece == 'k' || srcPiece == 'K'))
		{
			strcpy_s(ans, "1");
		}

		else if (!isalpha(dstPiece))
		{
			strcpy_s(ans, "0");
		}
	}

	if (!strcmp(ans, "0") || !strcmp(ans, "1"))
	{
		move(src, dest);
		this->_isWhiteTurn = !this->_isWhiteTurn;
	}

	return ans;
}


/*
setup board position + turn
In: none
Out: string
*/
string Board::setup()
{
	string strTurn = !this->_isWhiteTurn ? "1" : "0";
	string answer = this->_boardState + strTurn;

	return answer;
}

/*
gets the type of piece by pos
in: pos
out: type , NULL if out-of-bounds error
*/
char Board::getPieceInfo(string pos)
{
	char flag = NULL;

	int wid = pos.at(0) - 'a';//get length
	int len = pos.at(1) - '1';//get width

	if (0 <= wid && 7 >= wid && 0 <= len && 7 >= len)
	{
		flag = this->_boardState.at(wid + (BOARD_SIZE * len));
	}

	return flag;
}

/*
move piece from one index to another
In: src and dest
Out: None
*/

void Board::move(string src, string dest)
{
	// pieces type
	string srcStr(1, Board::getPieceInfo(src));
	string destStr(1, Board::getPieceInfo(dest));

	//indexs
	int srcIndex = ((src.at(0) - 'a') + (BOARD_SIZE * (src.at(1) - '1')));
	int destIndex = ((dest.at(0) - 'a') + (BOARD_SIZE * (dest.at(1) - '1')));

	if ((islower(Board::getPieceInfo(src)) && isupper(Board::getPieceInfo(dest))) || (isupper(Board::getPieceInfo(src)) && islower(Board::getPieceInfo(dest))))	//if there is a piece , MURDER IT!
	{
		(this->_boardState).replace(destIndex, 1, srcStr);//in dest, put src
		(this->_boardState).replace(srcIndex, 1, string("#"));
	}
	else
	{
		(this->_boardState).replace(srcIndex, 1, destStr);//in src, put dest
		(this->_boardState).replace(destIndex, 1, srcStr);//in dest, put src
	}

}

/*
Getter - board string
In: none
Out: string
*/
string Board::getBoardState()
{
	return this->_boardState;
}

void Board::UpdateBoard()
{
	int i = 0;
	int j = 0;

	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			this->_board[i][j] = this->_boardState.at(8 * i + j);
		}
	}
}

void Board::PrintBoard()
{
	int i = 0;
	int j = 0;

	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << this->_board[i][j] << " ";
		}
		std::cout << std::endl;
	}
}
