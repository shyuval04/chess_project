#include "Pawn.h"
#include "Board.h"

Pawn::Pawn(bool color)
{
	this->_color = color;
}

Pawn::~Pawn()
{

}


/*
checks if piece is able to reach
In: the source and destenation and the board
Out: if it's A legal move
*/
int Pawn::isAbleToReach(string src, string dest, Board b)
{
	this->calcWay(src, dest);

	bool turn = b.getTurn();

	bool canWhiteEat = (abs(this->_moveVector[0]) == 1) && (this->_moveVector[1] == 1) && (isalpha(b.getPieceInfo(dest)));
	bool canBlackEat = (abs(this->_moveVector[0]) == 1) && (this->_moveVector[1] == -1) && (isalpha(b.getPieceInfo(dest)));
	bool canEat = (canWhiteEat && turn) || (canBlackEat && !turn);

	bool moveRegularly = ((this->_moveVector[0] == 0) && ( (this->_moveVector[1] == 1 && turn) || (this->_moveVector[1] == -1 && !turn) ) && (b.getPieceInfo(dest) == '#'));
	
	bool firstMoveWhite = ( (this->_moveVector[0] == 0) && ((this->_moveVector[1] == 1 || this->_moveVector[1] == 2)) && !(isalpha(b.getPieceInfo(dest))));
	bool firstMoveBlack = ( (this->_moveVector[0] == 0) && ((this->_moveVector[1] == -1 || this->_moveVector[1] == -2)) && !(isalpha(b.getPieceInfo(dest))));
	bool isFirstLegal = ( (firstMoveWhite && turn && src.at(1) == '2' ) || (firstMoveBlack && !turn && src.at(1) == '7'));

	if (!canEat && !moveRegularly && !isFirstLegal)  /// !canEat && !moveRegularly && !isFirstLegal
	{
		return 6;
	}
	else
	{
		//if (!isalpha(b.getPieceInfo(dest)))
		//{
		//	return 6;
		//}

		if (this->_isFirst)
		{
			this->_isFirst = false;
		}

	}

	return 0;
}

/*
checks if the king aims the another king
in: the destintion and the board
out: if this king is aiming the another king
*/

bool Pawn::isAimingKing(string dst, Board b)
{
	bool flag = false;
	char kingTocheck = (this->_color) ? 'K' : 'k';

	if (b.getPieceInfo(dst) == kingTocheck)
	{
		flag = true;
	}

	return flag;
}
