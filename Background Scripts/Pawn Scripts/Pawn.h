#pragma once

#include "BasicPawn.h"

class Pawn : public BasicPawn
{
public:
	Pawn(bool color);
	~Pawn();

	bool isAimingKing(string src, Board b);
	int isAbleToReach(string src, string dest, Board b);

private:

	bool _isFirst = true;
};