#include "BasicPawn.h"


BasicPawn::BasicPawn(bool color)
{
	this->_color = color;
}

BasicPawn::~BasicPawn()
{
}

/*
checks if piece is able to reach
In: the source and destenation
Out: the way she passes
*/

int BasicPawn::isAbleToReach (string src, string dest, Board b)
{
	int flag = 0;
	int pos = 0;

	string* way = calcWay(src, dest);
	string cpy = src;

	std::cout << "The Way This Pawn Does it: \n" << &way << std::endl;

	while (pos < (way->size() - AXISES_AMOUNT)) //in order to ignore the last piece(in case he is eating)
	{
		cpy = way->substr(pos, AXISES_AMOUNT);

		if (isalpha(b.getPieceInfo(cpy)))
		{
			flag = 6;
			break;
		}
		pos += AXISES_AMOUNT;
	}

	return flag;
}

void BasicPawn::Kill()
{
	this->_isAlive = false;
}


/*
checks where the way the piece will go in order to get to her dest
In: the source and destenation
Out: the way she passes
*/
string* BasicPawn::calcWay(string src, string dest)
{
	this->GetMovment(src, dest);

	unsigned int size = abs(this->_moveVector[0]) + abs(this->_moveVector[1]) + 1;
	
	string* path = new string [size];
	string copy = src;

	int x = this->_moveVector[1];
	int y = this->_moveVector[0];

	if (y > 0)
	{
		while (y-- > 0)
		{
			copy.at(0)++;
			path->append(copy);

		}
	}
	else if (y < 0)
	{
		while (y++ < 0)
		{
			copy.at(0)--;
			path->append(copy);

		}
	}
	
	if (x > 0)
	{
		while (x-- > 0)
		{
			copy.at(1)++;
			path->append(copy);

		}
	}
	else if (x < 0)
	{
		while (x++ < 0)
		{
			copy.at(1)--;
			path->append(copy);

		}
	}

	return path;
	
}

/*
sets the vector movement to the chosen movements
In: the source and destenation
Out: none
*/
void BasicPawn::GetMovment(string src, string dest)
{
	int xVec = dest.at(0) - src.at(0);//get length
	int yVec = dest.at(1) - src.at(1);//get width

	this->_moveVector[0] = xVec;
	this->_moveVector[1] = yVec;
}