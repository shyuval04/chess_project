#ifndef BASICPH
#define BASICPH

#include <iostream>
#include <math.h>
#include "Board.h"

#define AXISES_AMOUNT 2

using std::string;

class BasicPawn
{

public:

	BasicPawn(bool color = true);
	virtual ~BasicPawn();
	void GetMovment(string src, string dest); //from forthend
	int isAbleToReach(string src, string dest, Board b);
	void Kill();//if is getting eaten
	string* calcWay(string src, string dest); //calc the way to the destination


private:
	bool _isAlive = true;
	bool _color;
	bool _isSolid = true;
	int _moveVector[AXISES_AMOUNT];//X Movment, Y movment

};
#endif // !BASICPH
