#pragma once

#include "BasicPawn.h"

class King : public BasicPawn
{
public:
	King();
	~King();

	int isAbleToReach(string src, string dest, Board b);
	bool isAimingKing(string dst, Board b);
	void isThreatened();

private:
	bool isInDanger = false;
};
#pragma once
#pragma once
