#include "King.h"
#include "Board.h"

King::King()
{

}

King::~King()
{

}


/*
checks if piece is able to reach
In: the source and destenation and the board
Out: if it's A legal move
*/
int King::isAbleToReach(string src, string dest, Board b)
{
	this->calcWay(src, dest);

	if ((abs(this->_moveVector[0]) > 1) || (abs(this->_moveVector[1]) > 1))
	{
		return 6;
	}

	else
	{
		if (!isalpha(b.getPieceInfo(dest)))
		{
			return 6;
		}

	}

	return 0;
}

/*
checks if the king aims the another king
in: the destintion and the board
out: if this king is aiming the another king
*/

bool King::isAimingKing(string dst, Board b)
{
	char kingTocheck = (this->_color) ? 'K' : 'k';

	if (b.getPieceInfo(dst) == kingTocheck)
	{
		return true;
	}

	return false;
}

///if the king is Threatened than change it to true
void King::isThreatened()
{
	this->isInDanger = true;
}
