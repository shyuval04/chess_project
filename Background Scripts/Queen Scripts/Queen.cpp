﻿#include "Queen.h"
#include "Board.h"


Queen::Queen()
{

}

Queen::~Queen()
{

}

/*
checks if Queen is able to reach
In: the source and destenation
Out: the way she passes
*/
int Queen::isAbleToReach(string src, string dest, Board b)
{
	int flag = 0;
	int pos = 0;

	string* way = calcWay(src, dest);
	string cpy = src;

	std::cout << "The Way This Queen Does it: \n" << way << std::endl;

	while (pos < (way->size() - AXISES_AMOUNT)) //in order to ignore the last piece(in case he is eating)
	{
		cpy = way->substr(pos, AXISES_AMOUNT);
		if (isalpha(b.getPieceInfo(cpy)))
		{
			flag = 6;
			break;
		}
		pos += AXISES_AMOUNT;
	}

}


/*
this function check if the queen is Aiming the king
input: the board and the src square
output: boolean variable - if the queen is Aiming the king
*/
bool Queen::isAimingKing(string src, Board b)
{
	bool IsAiming = false;

	char kingTocheck = (this->_color) ? 'K' : 'k';
	char i = 0;

	char posY = src.at(1);
	char posX = src.at(0);

	char posYcpy = posY + 1;
	char posXcpy = posX + 1;

	string stringToCheck = "/0";
	char pieceToCheck = 0;

	while (--posXcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";
	posXcpy = posX - 1;

	while (++posXcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";

	while (--posYcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	stringToCheck = "/0";
	posYcpy = posY - 1;

	while (++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	////////////////
	

	//Diagonal right up
	posYcpy = posY - 1;
	posXcpy = posX - 1;

	while (++posXcpy <= '8' && ++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posXcpy + posY + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	//Diagonal down right

	posXcpy = posX - 1;
	posYcpy = posY + 1;
	while (++posXcpy <= '8' && --posYcpy >= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}

	//Diagonal up and left

	posXcpy = posX - 1;
	posYcpy = posY + 1;
	while (--posXcpy >= '1' && ++posYcpy <= '8' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}



	//Diagonal down to the left
	posXcpy = posX - 1;
	posYcpy = posY - 1;
	while (--posXcpy >= '1' && --posYcpy <= '1' && !IsAiming)
	{
		stringToCheck = string() + posX + posYcpy + string();
		pieceToCheck = b.getPieceInfo(stringToCheck);

		if (pieceToCheck == kingTocheck)
		{
			IsAiming = true;
			break;
		}
		else if (isalpha(pieceToCheck))
		{
			break;
		}
	}


	return IsAiming;
}



