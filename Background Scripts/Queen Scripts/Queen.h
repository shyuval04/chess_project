#pragma once

#include "BasicPawn.h"

class Queen : public BasicPawn
{
private:



public:
	Queen();
	~Queen();
	int isAbleToReach(string src, string dest, Board b);
	bool isAimingKing(string src, Board b);

};