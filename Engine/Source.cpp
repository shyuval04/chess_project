
#include "Pipe.h"
#include "Board.h"
#include "BasicPawn.h"

#include <iostream>
#include <thread>

using namespace std;
void main()
{
	srand(time_t(NULL));
	
	
	Pipe p;
	Board b;
	BasicPawn bp;

	bool isConnect = p.connect();
	string order = b.setup();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, order.c_str());
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{

		strcpy_s(msgToGraphics, b.getAnswer(msgFromGraphics)); // msgToGraphics should contain the result of the operation

		// board as string
		//std::cout << "String Order Of Board:\n" << b.getBoardState() << "\n" << std::endl;

		b.UpdateBoard();

		//board as 2d arr
		std::cout << "Table of the Board:" << std::endl << std::endl ;
		b.PrintBoard();
		std::cout << std::endl;


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}